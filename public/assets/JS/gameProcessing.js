function debounce(func, wait, immediate) {
    var timeout;
    return function () {
        var context = this, args = arguments;
        var later = function () {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
}

function once(fn, context) {
    var result;
    return function () {
        if (fn) {
            result = fn.apply(context || this, arguments);
            fn = null;
        }
        return result;
    };
}

function gameProcessing(urlGame, token, keyRoom, chatConn) {
    let conn = new WebSocket(`${urlGame}/chat?token=${token}&key=${keyRoom}`);
    gf = new gameFacade(UnityLoader.instantiate("gameContainer", "WebGLTogusCorgool/Build/WebBuild.json", {onProgress: UnityProgress}));
    $('#game').toggleClass('close-www');
    let isBlockCells = true;
    const sendToServerReady = () => {
        conn.send(JSON.stringify({type: 'toStepReady'}));
    };

    const ready = debounce(function () {
        console.log(`Визуализация готова к работе`);
        gf.toGame.lockCells();
        conn.send(JSON.stringify({type: 'game_ready'}));
    }, 1000);

    $('#btn11').on('click', ()=>{
        console.log('==>',evens.getStusRunung('gameStep'), evens.getQueueLen('showWindow'));
    });

    function observer(){
        const lisener = {};

        this.getLisener = ()=>JSON.stringify(lisener);

        this.getQueueLen = (name)=>{
            if(!lisener[name]) return null;
            return lisener[name]['fn'].length;
        };

        this.getStusRunung = (name)=>{
            if(!lisener[name]) return null;
            return !lisener[name]['fn'].length && !lisener[name]['run'];
        };

        this.add = (name, fn)=>{
            if(!lisener[name]) lisener[name] = {'fn':[], 'run': false};
            lisener[name]['fn'].push(fn);
        };

        this.next = (name)=>{
            console.log('next', name, lisener);
            if(!lisener[name]) return;
            lisener[name]['run'] = !!lisener[name]['fn'].length;
            run(name);
        };

        this.start = (name)=>{
            if(!lisener[name]) return;
            if(!lisener[name]['fn'].length) return;
            if(lisener[name]['run']) return;
            lisener[name]['run'] = true;
            run(name);
        };

        const run = (name)=>{
            const fnFirst = lisener[name]['fn'].shift();
            if(fnFirst) fnFirst();
        }
    }

    const evens = new observer();
    gf.setHookOnGameReady(function () {
        ready();
    });

    gf.setHookOnStateIsSet(function () {
        // console.log(`Состояние игры установлено`);
    });

    gf.setHookOnCellClick(function (boardSide, cellNumber) {
        gf.toGame.lockCells();
        conn.send(JSON.stringify({type: 'step', cell_number: cellNumber}));
        // console.log(`Нажатие на ячейку ${cellNumber} со стороны ${boardSide}`);
    });

    gf.setHookOnAlertClosed(function () {
        // console.log(`Панель уведомления была закрыта`);
    });

    function sendDataAboutReady() {
        conn.send(JSON.stringify({type: 'stepVisualReady'}));
    }
    let isEndGame = false;
    let endGameData = null;
    gf.setHookOnPlayActionsExecuted(function () {
        console.log(`Игровые действия были завершены`);
        // console.log(evens.getLisener(),evens.get('gameStep'), evens.get('showWindow'));
        // if(endGameData && (evens.getStusRunung('gameStep') && evens.getQueueLen('showWindow'))) showResultModalOfGame(endGameData);
        evens.next('showWindow');
        sendDataAboutReady();
        // console.log(evens.lisener);
    });

    function setInitialState(stateData) {
        gf.toGame.setGameState(stateData);
    }

    function unlockNextStep(isBlockCells) {
        if (isBlockCells) {
            showModalAboutEnemyStep()
        } else {
            showModalAboutYourStep(sendToServerReady)
        }
    }

    function showModalAboutYourStep(callback) {
        var canOnlyFireOnce = once(function () {
            callback()
        });
        setTimeout(function () {
            canOnlyFireOnce();
        }, 5000);
        swal({
            title: 'Ваш ход',
            allowOutsideClick: false,
            allowEscapeKey: false,
            allowEnterKey: false
        }).then(() => {
            gf.toGame.unlockCells();
            canOnlyFireOnce();
            // console.log('evens',evens.getLisener(),evens.get('gameStep'), evens.get('showWindow'));

            evens.next('gameStep');
        })
    }

    function showModalAboutEnemyStep() {
        swal({
            title: 'Ход противника',
            timer: 5000,
            allowOutsideClick: true,
            confirmButtonText: 'OK',
            allowEscapeKey: false,
            allowEnterKey: false
        }).then(()=>{
            evens.next('gameStep');
        })
    }

    function showResultModalOfGame(data) {
        chatConn.send(JSON.stringify({type: 'endGame', room: keyRoom}));
        let result = '';
        let reason;
        if(data.type === 'abortGame') {
            reason = "Ваш противник покинул игру"
        } else if(data.type === 'endGame') {
            result = `<h3>Результат:</hr><br>`;
            if(data.resultGame === 'won') {
                reason = "Вы выиграли!"
            } else if(data.resultGame === 'lose') {
                reason = "Вы проиграли!"
            } else {
                reason = 'Ничья!'
            }
        }
        swal({
            type: 'info',
            title: 'Игра закончена',
            html: `${result}<p>${reason}</p>`,
            confirmButtonText: 'OK',
        }).then(() => {
            location.reload();
        })
    }

    function giveTheFirstMove(side_color) {
        if (side_color === 'White') {
            console.log('твой первый ход');
            showModalAboutYourStep(sendToServerReady)
        } else {
            console.log('первый ход противника');
            showModalAboutEnemyStep()
        }
    }

    function startGameInterval(data) {
        let side_color = data.state_data.Side;
        let timerInterval;
        let end = data.game_start_time * 1000;
        let date = new Date().getTime();
        let difference = end - date;
        console.log(end, date, end - date);
        swal({
            html: '<h1 style="font-size: 50px">Игра начнется через <br><strong style="font-size: 70px"></strong></h1>',
            timer: difference > 0 ? difference : 1000,
            allowOutsideClick: false,
            allowEscapeKey: false,
            allowEnterKey: false,
            onOpen: () => {
                swal.showLoading();
                timerInterval = setInterval(() => {
                    swal.getContent().querySelector('strong')
                        .textContent = swal.getTimerLeft()
                }, 10);
                if (parseInt(swal.getTimerLeft(), 10) <= 0) {
                    clearInterval(timerInterval);
                }
            },
            onClose: () => {
                clearInterval(timerInterval)
            }
        }).then(() => {
            console.log('вызов функции о первом ходе')
            giveTheFirstMove(side_color)
        })
    }

    conn.onopen = function (e) {
        console.info("Connection established succesfully");
    };
    conn.onmessage = function (e) {
        let data = JSON.parse(e.data);
        console.log('data from server', data);
        if (data.type === 'set_state') {
            setInitialState(data.state_data);
            startGameInterval(data)
        } else if(data.type === 'click_again'){
            gf.toGame.unlockCells();
        } else if (data.type === 'step') {
            evens.add('gameStep', ()=>{
                gf.toGame.executePlayActions(data.data_step);
            });
            evens.add('showWindow', ()=>{
                unlockNextStep(data.isBlockCells);
            });
            evens.start('gameStep');
        } else if(data.type === 'endGame') {
            isEndGame = true;
            endGameData = data;
            const endGameTimer = setInterval(()=>{
                console.log('endGame', evens.getLisener(),evens.getStusRunung('gameStep'), !evens.getQueueLen('showWindow'));
                if(evens.getStusRunung('gameStep') && !evens.getQueueLen('showWindow')) {
                    console.log('end show windows');
                    showResultModalOfGame(endGameData);
                    clearInterval(endGameTimer);
                }
            }, 1000);
        } else if(data.type === 'abortGame' && !isEndGame) {
            showResultModalOfGame(data);
        }
    };

    conn.onerror = function (e) {
        console.error(e);
    };

    conn.onclose = function (e) {
        console.error(e);
    };
}
