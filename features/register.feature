#language: ru

  Функционал: Проверяем функционал регистрации пользователя в приложении (прим. Выбранная локаль - RU)
    Сценарий: Регистрируем нового пользователя
      Допустим я перехожу на страницу 'fos_user_registration_register'
      И я вижу 'css' элемент 'fos_user_registration_form'
      Тогда я заполняю поле 'fos_user_registration_form[email]' значением 'any.root@root.root'
      И я заполняю поле 'fos_user_registration_form[username]' значением 'any_root'
      И я заполняю поле 'fos_user_registration_form[plainPassword][first]' значением 'root'
      И я заполняю поле 'fos_user_registration_form[plainPassword][second]' значением 'root'
      И я заполняю поле 'fos_user_registration_form[fullName]' значением 'Root Root'
      И нажимаю кнопку '_submit'
      Тогда я вижу 'text' элемент 'Поздравляем any_root, ваш аккаунт подтвержден'

    Сценарий: Регистрация пользователя с уже существующим Email
      Допустим я перехожу на страницу 'fos_user_registration_register'
      И я вижу 'css' элемент 'fos_user_registration_form'
      Тогда я заполняю поле 'fos_user_registration_form[email]' значением 'crocodiller227@gmail.com'
      И я заполняю поле 'fos_user_registration_form[username]' значением 'crocodiller227'
      И я заполняю поле 'fos_user_registration_form[plainPassword][first]' значением 'root'
      И я заполняю поле 'fos_user_registration_form[plainPassword][second]' значением 'root'
      И я заполняю поле 'fos_user_registration_form[fullName]' значением 'Root Root'
      И нажимаю кнопку '_submit'
      Тогда я вижу 'text' элемент 'Email уже используется'