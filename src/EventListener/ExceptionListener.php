<?php

namespace App\EventListener;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

class ExceptionListener
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {

        $this->container = $container;
    }

    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();

        $response = new Response();
        $render = $this->container->get('templating');
        if ($exception instanceof HttpExceptionInterface) {
            $code = $exception->getStatusCode();
            if ($code == 404){
                $template = $render->render('bundles/TwigBundle/Exception/error404.html.twig');
                $response->setContent($template);
            } elseif ($code == 403){
                $template = $render->render('bundles/TwigBundle/Exception/error403.html.twig');
                $response->setContent($template);
            } else {
                $response->setStatusCode($exception->getStatusCode());
                $response->headers->replace($exception->getHeaders());
            }
        } else {
            $template = $render->render('bundles/TwigBundle/Exception/error500.html.twig');
            $response->setContent($template);
        }
        $event->setResponse($response);
    }
}

